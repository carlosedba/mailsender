<?php
namespace Rise;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use Rise\Utils\IdGenerator;

use Rise\Models\MSView;

$net->get('/', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'index', [
    "hero" => [],
    "quem-somos" => [],
    "o-que-oferecemos" => [],
    "noticias" => [],
  ]);
});

$net->get('/{msEmailId}/{msUserId}', function (Request $request, Response $response, $args) {
  $msEmailId = $args['msEmailId'];
  $msUserId = $args['msUserId'];

  $view = Model::factory('MSView')->create(array(
    'id'                => IdGenerator::uniqueId(8),
    'ms_email_id'       => $msEmailId,
    'ms_user_id'        => $msUserId,
    'accessed_at'       => date('Y-m-d H:i:s'),
  ));

  /*
  $image = file_get_contents('/var/www/html/mylikes.app/public_html/ms/public_html/img.png');
  $finfo = new finfo(FILEINFO_MIME_TYPE);
  $response->withHeader('Content-Type', 'content-type: ' . $finfo->buffer($image));
  */

  if ($view->save()) {
    $response = $response->withStatus(201);
  } else {
    $response = $response->withStatus(400);
  }
});

?>

