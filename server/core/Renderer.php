<?php

namespace Rise;

use Sabre\Uri;

class Renderer
{
	private $config;
	private $store;
	private $internalRenderer;
	private $themeRenderer;

	public function __construct(array $config = [], Store $store)
	{
		$this->config = $config;
		$this->store = $store;

		$this->setInternalRenderer();
		$this->setThemeRenderer();
		$this->applyDirectives();
	}

	public function internal()
	{
		return $this->internalRenderer;
	}

	public function theme()
	{
		return $this->themeRenderer;
	}

	public function setInternalRenderer()
	{
		$templates = $this->config['internal']['templates'];
		$cache = $this->config['internal']['cache'];

		$this->internalRenderer = new Blade($templates, $cache);
	}

	public function setThemeRenderer()
	{
		$theme = $this->store->get('config.theme');
		$templates = $this->config['theme']['templates'] . $theme;
		$cache = $this->config['theme']['cache'];

		$this->themeRenderer = new Blade($templates, $cache);
	}

	private function applyDirectives()
	{
		$store = $this->store;

		$this->themeRenderer->directive('set', function ($expression) {
			list($variable, $value) = explode(', ', str_replace(['(', ')'], '', $expression));
        	return "<?php {$variable} = {$value}; ?>";
        });

		$this->themeRenderer->directive('store', function ($expression) use ($store) {
	        $expression = $this->themeRenderer->stripQuotes($expression);
            return $store->get($expression);
        });

		$this->themeRenderer->directive('asset', function ($expression) use ($store) {
	        $expression = $this->themeRenderer->stripQuotes($expression);
			$siteUrl = $store->get('config.siteUrl');
			$theme = $store->get('config.theme');

            return Uri\normalize("{$siteUrl}/themes/{$theme}/${expression}");
        });

		/*$theme->directive('set', function ($value) use ($store) {
			$values = array_map('trim', explode(',', $value));
			$item = $store->get('config.siteName');
			return $this->compileString("@section({$values[0]}, '{$item}')");
        });*/
	}
}
?>
