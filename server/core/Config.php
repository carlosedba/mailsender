<?php

namespace Rise;

use \Psr\Container\ContainerInterface;
use Rise\Model;

class Store implements ContainerInterface
{
	protected $items = array();
	
	function __construct()
	{
		$config = Model::factory('Config');
		$data = $config->select('name')
				->select('value')
				->where('autoload', true)
				->findArray();
		$this->items = array_merge($this->items, ['config' => $data]);
	}

	public function all()
	{
		return $this->data;
	}

	public function get($key)
	{
		
	}

	public function has($id)
	{

	}
}
?>