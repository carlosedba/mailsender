<?php
namespace Rise;

require_once('Bootstrap.php');

use \PHPMailer;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Utils\IdGenerator;
use Rise\Auth\JWT;
use Rise\Models\User;

$container = new Container($config);
$net = new Netcore($container);

$net->add(new JWT([
  "secure" => false,
  "header" => "Authorization",
  "signature" => "RSA",
  "algorithm" => "SHA256",
  "key" => PUBLIC_KEY,
  "path" => ["/admin", "/api"],
  "ignore" => ["/api/v1/token", "/email/contato"],
  "callback" => function ($request, $response, $args) use ($container) {
    $container->set('token', $args['token']);
    return $response;
  },
  "error" => function ($request, $response, $args) {
    return $response;
  }
]));


/* ****************************** */
/* ************ API ************* */
/* ****************************** */

$net->group('/api/ms', function () {
  $this->delete('/users/delete/{id}[/]',      constant('NAMESPACE') . '\Api\MSUsers:delete');
  $this->post('/users/update/{id}[/]',        constant('NAMESPACE') . '\Api\MSUsers:update');
  $this->post('/users/create[/]',             constant('NAMESPACE') . '\Api\MSUsers:create');
  $this->get('/users/email/{email}[/]',       constant('NAMESPACE') . '\Api\MSUsers:findOne');
  $this->get('/users/{id}[/]',                constant('NAMESPACE') . '\Api\MSUsers:findOneById');
  $this->get('/users[/]',                     constant('NAMESPACE') . '\Api\MSUsers:findAll');

  $this->delete('/groups/delete/{id}[/]',      constant('NAMESPACE') . '\Api\MSGroups:delete');
  $this->post('/groups/update/{id}[/]',        constant('NAMESPACE') . '\Api\MSGroups:update');
  $this->post('/groups/create[/]',             constant('NAMESPACE') . '\Api\MSGroups:create');
  $this->get('/groups/{slug}[/]',              constant('NAMESPACE') . '\Api\MSGroups:findOne');
  $this->get('/groups[/]',                     constant('NAMESPACE') . '\Api\MSGroups:findAll');

  $this->delete('/emails/delete/{id}[/]',        constant('NAMESPACE') . '\Api\MSEmails:delete');
  $this->post('/emails/update/{id}[/]',          constant('NAMESPACE') . '\Api\MSEmails:update');
  $this->post('/emails/create[/]',               constant('NAMESPACE') . '\Api\MSEmails:create');  
  $this->get('/emails/{id}[/]',                  constant('NAMESPACE') . '\Api\MSEmails:findOneById');
  $this->get('/emails[/]',                       constant('NAMESPACE') . '\Api\MSEmails:findAll');

  $this->delete('/links/delete/{id}[/]',        constant('NAMESPACE') . '\Api\MSLinks:delete');
  $this->post('/links/update/{id}[/]',          constant('NAMESPACE') . '\Api\MSLinks:update');
  $this->post('/links/create[/]',               constant('NAMESPACE') . '\Api\MSLinks:create');  
  $this->get('/links/{id}[/]',                  constant('NAMESPACE') . '\Api\MSLinks:findOneById');
  $this->get('/links[/]',                       constant('NAMESPACE') . '\Api\MSLinks:findAll');

  $this->delete('/accesses/delete/{id}[/]',   constant('NAMESPACE') . '\Api\MSAccesses:delete');
  $this->post('/accesses/update/{id}[/]',     constant('NAMESPACE') . '\Api\MSAccesses:update');
  $this->post('/accesses/create[/]',          constant('NAMESPACE') . '\Api\MSAccesses:create');  
  $this->get('/accesses/{id}[/]',             constant('NAMESPACE') . '\Api\MSAccesses:findOneById');
  $this->get('/accesses[/]',                  constant('NAMESPACE') . '\Api\MSAccesses:findAll');
});

$net->group('/api/v1', function () {
  $this->delete('/users/delete/{id}[/]',      constant('NAMESPACE') . '\Api\Users:delete');
  $this->post('/users/update/{id}[/]',        constant('NAMESPACE') . '\Api\Users:update');
  $this->post('/users/create[/]',             constant('NAMESPACE') . '\Api\Users:create');
  $this->get('/users/email/{email}[/]',       constant('NAMESPACE') . '\Api\Users:findOne');
  $this->get('/users/{id}[/]',                constant('NAMESPACE') . '\Api\Users:findOneById');
  $this->get('/users[/]',                     constant('NAMESPACE') . '\Api\Users:findAll');

  $this->delete('/config/delete/{id}[/]',     constant('NAMESPACE') . '\Api\Config:delete');
  $this->post('/config/update/{id}[/]',       constant('NAMESPACE') . '\Api\Config:update');
  $this->post('/config/create[/]',            constant('NAMESPACE') . '\Api\Config:create');  
  $this->get('/config/{id}[/]',               constant('NAMESPACE') . '\Api\Config:findOneById');
  $this->get('/config[/]',                    constant('NAMESPACE') . '\Api\Config:findAll');

  $this->delete('/attributes/delete/{id}[/]', constant('NAMESPACE') . '\Api\Attributes:delete');
  $this->post('/attributes/update/{id}[/]',   constant('NAMESPACE') . '\Api\Attributes:update');
  $this->post('/attributes/create[/]',        constant('NAMESPACE') . '\Api\Attributes:create');  
  $this->get('/attributes/{id}[/]',           constant('NAMESPACE') . '\Api\Attributes:findOneById');
  $this->get('/attributes[/]',                constant('NAMESPACE') . '\Api\Attributes:findAll');

  $this->delete('/fields/delete/{id}[/]',     constant('NAMESPACE') . '\Api\Fields:delete');
  $this->post('/fields/update/{id}[/]',       constant('NAMESPACE') . '\Api\Fields:update');
  $this->post('/fields/create[/]',            constant('NAMESPACE') . '\Api\Fields:create');  
  $this->get('/fields/{id}[/]',               constant('NAMESPACE') . '\Api\Fields:findOneById');
  $this->get('/fields[/]',                    constant('NAMESPACE') . '\Api\Fields:findAll');

  $this->delete('/collections/delete/{id}[/]',  constant('NAMESPACE') . '\Api\Collections:delete');
  $this->post('/collections/update/{id}[/]',    constant('NAMESPACE') . '\Api\Collections:update');
  $this->post('/collections/create[/]',         constant('NAMESPACE') . '\Api\Collections:create'); 
  $this->get('/collections/{id}[/]',            constant('NAMESPACE') . '\Api\Collections:findOneById');
  $this->get('/collections[/]',                 constant('NAMESPACE') . '\Api\Collections:findAll');

  $this->delete('/content/delete/{id}[/]',      constant('NAMESPACE') . '\Api\Content:delete');
  $this->post('/content/update/{id}[/]',        constant('NAMESPACE') . '\Api\Content:update');
  $this->post('/content/create[/]',             constant('NAMESPACE') . '\Api\Content:create'); 
  $this->get('/content/{id}[/]',                constant('NAMESPACE') . '\Api\Content:findOneById');
  $this->get('/content[/]',                     constant('NAMESPACE') . '\Api\Content:findAll');

  $this->delete('/pages/delete/{id}[/]',        constant('NAMESPACE') . '\Api\Page:delete');
  $this->post('/pages/update/{id}[/]',          constant('NAMESPACE') . '\Api\Page:update');
  $this->post('/pages/create[/]',               constant('NAMESPACE') . '\Api\Page:create');  
  $this->get('/pages/{id}[/]',                  constant('NAMESPACE') . '\Api\Page:findOneById');
  $this->get('/pages[/]',                       constant('NAMESPACE') . '\Api\Page:findAll');
});

$net->post('/api/v1/token', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody();
  $email = $data['email'];
  $password = $data['password'];
  $factory = Model::factory('User');

  if ($user = $factory->where('email', $email)->findOne()) {
    if ($user->password == hash('sha256', $password)) {
      $token = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, [
        'id' => $user->id,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
        'email' => $user->email,
        'picture' => $user->picture,
        'role' => $user->role,
      ])->__toString();
      $json = json_encode(array('token' => $token));
      $response->getBody()->write($json);
    } else {
      $json = json_encode(array(
        "error" => [
          "code" => 1001,
          "message" => "Invalid password."
        ]
      ));

      $response = $response->withStatus(401);
      $response->getBody()->write($json);
    }
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1002,
        "message" => "Email not found."
      ]
    ));

    $response = $response->withStatus(401);
    $response->getBody()->write($json);
  }

  return $response;
});

$net->post('/api/v1/token/renew', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody();
  $token = $data['token'];

  $isValid = JWT::verify('RSA', 'SHA256', PUBLIC_KEY, $token);
  $factory = Model::factory('User');

  if ($isValid) {
    if ($user = $factory->where('id', $isValid->getClaim('id'))->findOne()) {
      $newToken = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, [
        'id' => $user->id,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
        'email' => $user->email,
        'picture' => $user->picture,
        'role' => $user->role,
      ])->__toString();
      $json = json_encode(array('token' => $token));
      $response->getBody()->write($json);
    } else {
      $json = json_encode(array(
        "error" => [
          "code" => 1003,
          "message" => "Invalid token provided."
        ]
      ));
    }
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1003,
        "message" => "Invalid token provided."
      ]
    ));

    $response = $response->withStatus(401);
    $response->getBody()->write($json);
  }

  return $response;
});

$net->post('/api/v1/confirm/password/{id}', function (Request $request, Response $response, $args) {
  $id = $args['id'];

  $data = $request->getParsedBody();
  $password = $data['password'];

  $user = Model::factory('User')->where('id', $id)->findOne();

  if ($user->password == hash('sha256', $password)) {
    $response = $response->withStatus(200);
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1001,
        "message" => "Invalid password."
      ]
    ));
    $response->getBody()->write($json);
  }

  return $response;
});


/* ****************************** */
/* ******* Theme routes ********* */
/* ****************************** */

$manifest = Theme::getManifest($container->store);

foreach ($manifest->routes as $route) {
  $net->get($route->pattern, function (Request $request, Response $response, $args) use ($route) {
    return $this->theme->render($response, $route->layout, ($route->data) ? $route->data : []);
  });
}

/*
$net->get('/', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'index');
});
*/


/* ****************************** */
/* ********* DEV ROUTES ********* */
/* ****************************** */

$net->get('/dev/pwd', function (Request $request, Response $response, $args) {  
  $params = $request->getParams();
  $password = $params['password'];
  $hash = hash('sha256', $password);

  $response->getBody()->write($hash);
  return $response;
});

$net->get('/dev/id', function (Request $request, Response $response, $args) {
  $response->getBody()->write(IdGenerator::uniqueId(8));
  return $response;
});


/* ****************************** */
/* ******* CUSTOM ROUTES ******** */
/* ****************************** */

require_once('Routes.php');

$net->run();
?>
