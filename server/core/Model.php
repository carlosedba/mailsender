<?php
namespace Rise;

class Model extends \Model
{
    use Concerns\HidesAttributes,
        Concerns\GuardsAttributes;

    /**
     * Update the fillable attributes of the model dynamically.
     *
     * @param array $data
     * @return $this
     */
    public function fillAttributes(array $data = [])
    {
        foreach ($data as $key => $value) {
            if ($this->isFillable($key)) {
                switch ($key) {
                    case 'password':
                        $this->$key = hash('sha256', $value);
                        break;
                    
                    default:
                        $this->$key = $value;
                        break;
                }
            }
        }

        return $this;
    }

}
?>
