<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<meta name="description" content="@yield('description')">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	@section('css')
		@show
	@section('js')
		<script src="{{ $theme }}/vendor/modernizr-2.8.3.min.js"></script>
		<script src="{{ $theme }}/vendor/bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
		@show
</head>