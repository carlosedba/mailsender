<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class MSGroups
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$group = Model::factory('MSGroup')->findMany();

		if ($group) {
			$group = $group->asArray();
			$json = json_encode($group);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function findOne(Request $request, Response $response, $args)
	{
		$slug = $args['slug'];
		$group = Model::factory('MSGroup')->where('slug', $slug)->findOne();

		if ($group) {
			$group = $group->asArray();
			$json = json_encode($group);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$group = Model::factory('MSGroup')->where('id', $id)->findOne();

		if ($group) {
			$group = $group->asArray();
			$json = json_encode($group);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	
	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$group = Model::factory('MSGroup')->create(array(
			'id' 								=> IdGenerator::uniqueId(8),
			'slug' 							=> $data['slug'],
			'data' 							=> $data['data'],
		));

		if ($group->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$id = $args['id'];

		if (!empty($files['picture'])) {
			$picture = Upload::saveTo(USER_DATA_PATH, $files['picture']);
			$data['picture'] = $picture;
		}

		$group = Model::factory('MSGroup')->where('id', $id)->findOne();

		if ($group) {
			$group->fillAttributes($data);

			if ($group->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}
				
		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$group = Model::factory('MSGroup')->where('id', $id)->findOne();

		if ($group) {
			if ($group->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
