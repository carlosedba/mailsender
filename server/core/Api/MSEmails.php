<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class MSEmails
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$emails = Model::factory('MSEmail')->findArray();
		$json = json_encode($emails);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$email = Model::factory('MSEmail')->where('id', $id)->findOne();

		if ($email) {
			$email = $email->asArray();
			$json = json_encode($email);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	
	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$email = Model::factory('MSEmail')->create(array(
			'id' 						=> IdGenerator::uniqueId(8),
			'name' 					=> $data['name'],
			'subject' 			=> $data['subject'],
			'sender_name' 	=> $data['sender_name'],
			'sender_email' 	=> $data['sender_email'],
			'content' 			=> $data['content'],
			'slug' 					=> $data['slug'],
		));

		if ($email->save()) {
			$json = json_encode($email->asArray());
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$email = Model::factory('MSEmail')->where('id', $id)->findOne();

		if ($email) {
			$email->fillAttributes($data);

			if ($email->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}
				
		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$email = Model::factory('MSEmail')->where('id', $id)->findOne();

		if ($email) {
			if ($email->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
