<?php
namespace Rise\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class Fields
{
	public static function findAll(Request $request, Response $response, $args)
	{
		$fields = Model::factory('Field')->findArray();
		$json = json_encode($fields);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$fields = Model::factory('Field')->where('id', $id)->findOne()->asArray();
		$json = json_encode($fields);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}
	
	public static function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$field = Model::factory('Field')->create(array(
			'id' 			=> IdGenerator::uniqueId(8),
			'name' 			=> $data['name'],
			'description' 	=> $data['description'],
			'type' 			=> $data['type'],
			'attribute_id' 	=> $data['attribute_id'],
			'collection_id' => $data['collection_id'],
		));

		if ($field->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$field = Model::factory('Field')
				->where('id', $id)
				->findOne()
				->fillAttributes($data);

		if ($field->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$field = Model::factory('Field')->where('id', $id)->findOne();

		if ($field->delete()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
