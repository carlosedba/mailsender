<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class MSAccess
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$access = Model::factory('MSAccess')->findMany();

		if ($access) {
			$access = $access->asArray();
			$json = json_encode($access);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$access = Model::factory('MSAccess')->where('id', $id)->findOne();

		if ($access) {
			$access = $access->asArray();
			$json = json_encode($access);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	
	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$access = Model::factory('MSAccess')->create(array(
			'id' 								=> IdGenerator::uniqueId(8),
			'ms_link_id' 				=> $data['ms_link_id'],
			'ms_user_id' 				=> $data['ms_user_id'],
			'accessed_at' 			=> date('Y-m-d H:i:s'),
		));

		if ($access->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$id = $args['id'];

		if (!empty($files['picture'])) {
			$picture = Upload::saveTo(USER_DATA_PATH, $files['picture']);
			$data['picture'] = $picture;
		}

		$access = Model::factory('MSAccess')->where('id', $id)->findOne();

		if ($access) {
			$access->fillAttributes(array_merge($data, [ "accessed_at" => date('Y-m-d H:i:s') ]));

			if ($access->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}
				
		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$access = Model::factory('MSAccess')->where('id', $id)->findOne();

		if ($access) {
			if ($access->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
