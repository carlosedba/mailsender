<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class MSLinks
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$links = Model::factory('MSLink')->findArray();
		$json = json_encode($links);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$link = Model::factory('MSLink')->where('id', $id)->findOne();

		if ($link) {
			$link = $link->asArray();
			$json = json_encode($link);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	
	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$link = Model::factory('MSLink')->create(array(
			'id' 						=> IdGenerator::uniqueId(8),
			'ms_email_id' 	=> $data['ms_email_id'],
			'name' 					=> $data['name'],
			'description' 	=> $data['description'],
			'slug' 					=> $data['slug'],
			'content' 			=> $data['content'],
			'created_at' 		=> date('Y-m-d H:i:s'),
			'updated_at' 		=> date('Y-m-d H:i:s'),
		));

		if ($link->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$link = Model::factory('MSLink')->where('id', $id)->findOne();

		if ($link) {
			$link->fillAttributes(array_merge($data, [ "updated_at" => date('Y-m-d H:i:s') ]));

			if ($link->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}
				
		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$link = Model::factory('MSLink')->where('id', $id)->findOne();

		if ($link) {
			if ($link->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
