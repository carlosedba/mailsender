<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class MSViews
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$view = Model::factory('MSView')->findMany();

		if ($view) {
			$view = $view->asArray();
			$json = json_encode($view);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$view = Model::factory('MSView')->where('id', $id)->findOne();

		if ($view) {
			$view = $view->asArray();
			$json = json_encode($view);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	
	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$view = Model::factory('MSView')->create(array(
			'id' 								=> IdGenerator::uniqueId(8),
			'ms_email_id' 			=> $data['ms_email_id'],
			'ms_user_id' 				=> $data['ms_user_id'],
			'accessed_at' 			=> date('Y-m-d H:i:s'),
		));

		if ($view->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$id = $args['id'];

		if (!empty($files['picture'])) {
			$picture = Upload::saveTo(USER_DATA_PATH, $files['picture']);
			$data['picture'] = $picture;
		}

		$view = Model::factory('MSView')->where('id', $id)->findOne();

		if ($view) {
			$view->fillAttributes(array_merge($data, [ "accessed_at" => date('Y-m-d H:i:s') ]));

			if ($view->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}
				
		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$view = Model::factory('MSView')->where('id', $id)->findOne();

		if ($view) {
			if ($view->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
