<?php
namespace Rise\Models;

use Rise\Model;

class MSGroup extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_ms_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'slug', 'data'
    ];
}
?>