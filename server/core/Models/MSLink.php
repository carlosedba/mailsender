<?php
namespace Rise\Models;

use Rise\Model;

class MSLink extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_ms_links';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'ms_email_id', 'name', 'description', 'slug', 'content', 'created_at', 'updated_at'
    ];

    public function msEmail()
    {
        return $this->belongsTo('MSEmail', 'ms_email_id');
    }
}
?>