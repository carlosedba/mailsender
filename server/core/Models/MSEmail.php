<?php
namespace Rise\Models;

use Rise\Model;

class MSEmail extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_ms_emails';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'subject', 'sender_name', 'sender_email', 'content', 'slug'
    ];
}
?>