<?php
namespace Rise\Models;

use Rise\Model;

class Field extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_fields';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'type', 'attribute_id', 'collection_id',
    ];

    public function attribute()
    {
        return $this->belongsTo('Attribute', 'attribute_id');
    }

    public function collection()
    {
        return $this->belongsTo('Collection', 'collection_id');
    }
}
?>