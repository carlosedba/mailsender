<?php
namespace Rise\Models;

use Rise\Model;

class MSView extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_ms_views';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'ms_email_id', 'ms_user_id', 'accessed_at'
    ];

    public function msEmail()
    {
        return $this->belongsTo('MSEmail', 'ms_email_id');
    }

    public function msUser()
    {
        return $this->belongsTo('MSUser', 'ms_user_id');
    }
}
?>