<?php
namespace Rise\Models;

use Rise\Model;

class MSAccess extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_ms_accesses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'ms_link_id', 'ms_user_id', 'accessed_at'
    ];

    public function msLink()
    {
        return $this->belongsTo('MSLink', 'ms_link_id');
    }

    public function msUser()
    {
        return $this->belongsTo('MSUser', 'ms_user_id');
    }
}
?>