<?php
namespace Rise\Models;

use Rise\Model;

class Page extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_collections';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'slug', 'collection_id',
    ];

    public function collection()
    {
        return $this->belongsTo('Collection', 'collection_id');
    }
}
?>