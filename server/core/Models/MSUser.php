<?php
namespace Rise\Models;

use Rise\Model;

class MSUser extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_ms_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'first_name', 'last_name', 'email'
    ];
}
?>