<?php $__env->startSection('title'); ?>
  HMI - Honoris Mérito Institute
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description'); ?>
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-css'); ?>
  ##parent-placeholder-97688f63ed1a87ba587e78933c42edf42ecae775##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('application-css'); ?>
  ##parent-placeholder-b51d72c3ca446ab0f6f653f45ff8b7eb92a61211##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fonts'); ?>
  ##parent-placeholder-04d3b602cdc8d51e1a3bb4d03f7dab96a9ec37e5##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  ##parent-placeholder-040f06fd774092478d450774f5ba30c5da78acc8##

  <section class="hero">
    <div class="hero-background" style="background-image: url('https://server.com/hmi/server/public_html//themes/hmi/resources/img/c095c302-3fbc-4ea0-833b-35088f7cb4aa.jpg');"></div>
    <div class="hero-overlay"></div>
    <div class="hero-content">
      <div class="hero-content-wrapper">
        <p class="hero-title">Remunerar diferente as entregas diferenciadas.</p>
      </div>
    </div>
  </section>

  <section class="section white section-one">
    <div class="section-wrapper">
      <div class="row">
        <div class="row-titles">
          <p class="row-title">Quem Somos</p>
          <p class="row-text">A Federação das Indústrias do Estado do Paraná (Fiep), em conjunto com os sindicatos do setor, trabalha pelo desenvolvimento sustentável das indústrias paranaenses. </p>
        </div>
        <div class="box box-one">
          <div class="box-content">
            <p>Nosso propósito é fomentar a cultura do mérito dentro das empresas.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section white section-two">
    <div class="row small-pad no-pad lr bottom">
      <div class="row-titles">
        <p class="row-title">O que oferecemos</p>
      </div>
      <div class="featured-container">
        <div class="featured featured-one">
          <div class="featured-background" style="background-image: url('https://server.com/hmi/server/public_html//themes/hmi/resources/img/jakob-dalbjorn-730178-unsplash-x1280-min.jpg')"></div>
          <div class="svg icon">
            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 600 444.1">
              <style>.iccongressos0{fill:#ffd4b0}.iccongressos1{fill:#77657e}.iccongressos2{fill:#edc4a2}.iccongressos3{fill:#ffdc64}.iccongressos4{fill:#fff082}</style><circle cx="300" cy="42.5" r="29.7" class="iccongressos0"/><path d="M230.6 201.1v-29.7c0-38.3 31.1-69.4 69.4-69.4 38.3 0 69.4 31.1 69.4 69.4v29.7H230.6z" class="iccongressos1"/><path d="M230.6 191.2v9.9h138.8v-29.7c0-18-7-35.3-19.5-48.2-25 42.1-70.3 68-119.3 68z" class="iccongressos1"/><path d="M315.4 17.2c2.9 4.6 4.4 10 4.4 15.4 0 16.4-13.3 29.7-29.7 29.7-5.4 0-10.8-1.5-15.4-4.4 8.5 14 26.7 18.5 40.7 10.1s18.5-26.7 10.1-40.7c-2.5-4.2-6-7.7-10.1-10.1z" class="iccongressos2"/><path d="M244.8 201.5h110.5v156.8H244.8z" class="iccongressos3"/><path d="M355.2 269H244.8c-3 0-5.5-2.5-5.5-5.5s2.5-5.5 5.5-5.5h110.5c3 0 5.5 2.5 5.5 5.5-.1 3-2.5 5.5-5.6 5.5zM355.2 254.5H244.8c-3 0-5.5-2.5-5.5-5.5s2.5-5.5 5.5-5.5h110.5c3 0 5.5 2.5 5.5 5.5s-2.5 5.5-5.6 5.5zM404.3 167.8l-1.2 3.6-3.8 11-8.9 25.6c-.8 2.4-3 3.9-5.5 3.9H215.2c-2.5 0-4.7-1.6-5.5-3.9l-8.9-25.6-3.8-11-1.2-3.6c-1.3-3.8 1.5-7.8 5.5-7.8h197.4c4 0 6.9 4 5.6 7.8zM473.3 429.1H126.7c-3.2 0-5.9-6.5-5.9-14.4v-49.5c0-8 2.6-14.4 5.9-14.4h346.6c3.2 0 5.9 6.5 5.9 14.4v49.5c0 7.9-2.7 14.4-5.9 14.4z" class="iccongressos4"/><path d="M403 171.4l-3.8 11H200.8l-3.8-11z" class="iccongressos3"/><path d="M230.6 429.1v-29.7c0-38.3 31.1-69.4 69.4-69.4 38.3 0 69.4 31.1 69.4 69.4v29.7H230.6z" class="iccongressos1"/><path d="M230.6 419.2v9.9h138.8v-29.7c0-18-7-35.3-19.5-48.2-25 42.1-70.3 68-119.3 68zM448.7 429.1v-29.7c0-38.3 31.1-69.4 69.4-69.4 38.3 0 69.4 31.1 69.4 69.4v29.7H448.7z" class="iccongressos1"/><path d="M448.7 419.2v9.9h138.8v-29.7c0-18-7-35.3-19.5-48.2-25 42.1-70.3 68-119.3 68zM12.5 429.1v-29.7c0-38.3 31.1-69.4 69.4-69.4 38.3 0 69.4 31.1 69.4 69.4v29.7H12.5z" class="iccongressos1"/><path d="M12.5 419.2v9.9h138.8v-29.7c0-18-7-35.3-19.5-48.2-25 42.1-70.3 68-119.3 68z" class="iccongressos1"/><circle cx="300" cy="270.5" r="29.7" class="iccongressos0"/><path d="M315.4 245.2c2.9 4.6 4.4 10 4.4 15.4 0 16.4-13.3 29.7-29.7 29.7-5.4 0-10.8-1.5-15.4-4.4 8.5 14 26.7 18.5 40.7 10.1 14-8.5 18.5-26.7 10.1-40.7-2.5-4.2-6-7.6-10.1-10.1z" class="iccongressos2"/><circle cx="518.1" cy="270.5" r="29.7" class="iccongressos0"/><path d="M533.5 245.2c2.9 4.6 4.4 10 4.4 15.4 0 16.4-13.3 29.7-29.7 29.7-5.4 0-10.8-1.5-15.4-4.4 8.5 14 26.7 18.5 40.7 10.1 14-8.5 18.5-26.7 10.1-40.7-2.5-4.2-6-7.6-10.1-10.1z" class="iccongressos2"/><circle cx="81.9" cy="270.5" r="29.7" class="iccongressos0"/><path d="M97.3 245.2c2.9 4.6 4.4 10 4.4 15.4 0 16.4-13.3 29.7-29.7 29.7-5.4 0-10.8-1.5-15.4-4.4 8.5 14 26.7 18.5 40.7 10.1 14-8.5 18.5-26.7 10.1-40.7-2.5-4.2-6-7.6-10.1-10.1z" class="iccongressos2"/>
            </svg>
          </div>
          <p class="featured-title">Congressos</p>
          <p class="featured-subtitle">In-company Eventos Vendas</p>
        </div>
        <div class="featured featured-one">
          <div class="featured-background" style="background-image: url('https://server.com/hmi/server/public_html//themes/hmi/resources/img/jakob-dalbjorn-730178-unsplash-x1280-min.jpg')"></div>
          <div class="svg icon">
            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 600 511.1">
              <style>.icpalestras1{fill:#fff}.icpalestras4{fill:#ddd9d4}.icpalestras5{fill:#ffd4b0}.icpalestras6{fill:#77657e}.icpalestras7{fill:#edc4a2}</style><path fill="#b7e5ff" d="M91.8 13.1h416.4v119H91.8z"/><path d="M458.6 42.8h19.8v19.8h-19.8zM379.3 82.5h19.8v19.8h-19.8zM419 82.5h59.5v19.8H419z" class="icpalestras1"/><path fill="#ffdc13" d="M122.5 92c-4.6 0-8.3-3.7-8.3-8.3 0-3.3 2-6.3 5-7.6L177 51.4c1.7-.7 3.5-.9 5.2-.4l63.3 15.8 55.2-23.7c4.2-1.9 9 0 10.9 4.2 1.9 4.2 0 9-4.2 10.9-.1 0-.1.1-.2.1L249.6 83c-1.7.7-3.5.9-5.2.4L181 67.6l-55.2 23.7c-1 .4-2.2.7-3.3.7z"/><path d="M379.3 42.8h59.5v19.8h-59.5zM337.3 42.8H359v19.8h-21.7z" class="icpalestras1"/><path fill="#99c8e0" d="M478.4 42.8v19.8h-19.8V49.3c-19.1 12.8-39 24.5-59.5 35.1v18h-19.8v-8.1c-33.4 15.8-68.1 28.4-103.8 37.8h232.7v-119H507c-12.6 10.5-25.8 20.3-39.2 29.7h10.6zm0 59.5H419V82.5h59.5v19.8z"/><path d="M478.4 62.7V42.8h-10.7c-3.1 2.1-6.1 4.4-9.2 6.4v13.4h19.9zM399.1 102.3v-18c-6.6 3.4-13.1 6.7-19.8 9.9v8h19.8zM419 82.5h59.5v19.8H419z" class="icpalestras4"/><circle cx="300" cy="110.5" r="29.7" class="icpalestras5"/><path d="M230.6 497.1v-29.7c0-38.3 31.1-69.4 69.4-69.4 38.3 0 69.4 31.1 69.4 69.4v29.7H230.6zM230.6 269.1v-29.7c0-38.3 31.1-69.4 69.4-69.4 38.3 0 69.4 31.1 69.4 69.4v29.7H230.6z" class="icpalestras6"/><path d="M230.6 259.2v9.9h138.8v-29.7c0-18-7-35.3-19.5-48.2-25 42.1-70.3 68-119.3 68zM230.6 487.2v9.9h138.8v-29.7c0-18-7-35.3-19.5-48.2-25 42.1-70.3 68-119.3 68zM448.7 497.1v-29.7c0-38.3 31.1-69.4 69.4-69.4 38.3 0 69.4 31.1 69.4 69.4v29.7H448.7z" class="icpalestras6"/><path d="M448.7 487.2v9.9h138.8v-29.7c0-18-7-35.3-19.5-48.2-25 42.1-70.3 68-119.3 68zM12.5 497.1v-29.7c0-38.3 31.1-69.4 69.4-69.4 38.3 0 69.4 31.1 69.4 69.4v29.7H12.5z" class="icpalestras6"/><path d="M12.5 487.2v9.9h138.8v-29.7c0-18-7-35.3-19.5-48.2-25 42.1-70.3 68-119.3 68z" class="icpalestras6"/><path d="M315.4 85.2c2.9 4.6 4.4 10 4.4 15.4 0 16.4-13.3 29.7-29.7 29.7-5.4 0-10.8-1.5-15.4-4.4 8.5 14 26.7 18.5 40.7 10.1 14-8.5 18.5-26.7 10.1-40.7-2.5-4.2-6-7.6-10.1-10.1z" class="icpalestras7"/><circle cx="300" cy="338.5" r="29.7" class="icpalestras5"/><path d="M315.4 313.2c2.9 4.6 4.4 10 4.4 15.4 0 16.4-13.3 29.7-29.7 29.7-5.4 0-10.8-1.5-15.4-4.4 8.5 14 26.7 18.5 40.7 10.1 14-8.5 18.5-26.7 10.1-40.7-2.5-4.2-6-7.6-10.1-10.1z" class="icpalestras7"/><circle cx="518.1" cy="338.5" r="29.7" class="icpalestras5"/><path d="M533.5 313.2c2.9 4.6 4.4 10 4.4 15.4 0 16.4-13.3 29.7-29.7 29.7-5.4 0-10.8-1.5-15.4-4.4 8.5 14 26.7 18.5 40.7 10.1 14-8.5 18.5-26.7 10.1-40.7-2.5-4.2-6-7.6-10.1-10.1z" class="icpalestras7"/><circle cx="81.9" cy="338.5" r="29.7" class="icpalestras5"/><path d="M97.3 313.2c2.9 4.6 4.4 10 4.4 15.4 0 16.4-13.3 29.7-29.7 29.7-5.4 0-10.8-1.5-15.4-4.4 8.5 14 26.7 18.5 40.7 10.1s18.5-26.7 10.1-40.7c-2.5-4.2-6-7.6-10.1-10.1z" class="icpalestras7"/>
            </svg>
          </div>
          <p class="featured-title">Palestras</p>
          <p class="featured-subtitle">In-company Eventos Vendas</p>
        </div>
        <div class="featured featured-one">
          <div class="featured-background" style="background-image: url('https://server.com/hmi/server/public_html//themes/hmi/resources/img/jakob-dalbjorn-730178-unsplash-x1280-min.jpg')"></div>
          <div class="svg icon">
            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 600 600">
              <style>.icconsultoria0{fill:#77657e}.icconsultoria1{fill:#ffd4b0}.icconsultoria2{fill:#edc4a2}.icconsultoria5{fill:#99c8e0}</style><path d="M12.6 592.9V545c0-61.7 50.1-111.8 111.8-111.8 61.7 0 111.8 50.1 111.8 111.8v47.9H12.6z" class="icconsultoria0"/><path d="M12.6 576.9v16h223.6V545c0-29-11.3-56.9-31.5-77.7-40.1 67.9-113.2 109.6-192.1 109.6zM364 592.9V545c0-61.7 50.1-111.8 111.8-111.8 61.7 0 111.8 50.1 111.8 111.8v47.9H364z" class="icconsultoria0"/><path d="M364 576.9v16h223.6V545c0-29-11.3-56.9-31.5-77.7C516 535.2 442.9 576.9 364 576.9z" class="icconsultoria0"/><circle cx="124.4" cy="337.3" r="47.9" class="icconsultoria1"/><path d="M149.2 296.5c4.6 7.5 7.1 16.1 7.1 24.8 0 26.5-21.5 47.9-47.9 47.9-8.8 0-17.4-2.5-24.8-7.1 13.6 22.6 43 29.8 65.6 16.2 22.6-13.6 29.8-43 16.2-65.6-4-6.6-9.5-12.2-16.2-16.2z" class="icconsultoria2"/><circle cx="475.8" cy="337.3" r="47.9" class="icconsultoria1"/><path d="M500.6 296.5c4.6 7.5 7.1 16.1 7.1 24.8 0 26.5-21.5 47.9-47.9 47.9-8.8 0-17.4-2.5-24.8-7.1 13.6 22.6 43 29.8 65.6 16.2 22.6-13.6 29.8-43 16.2-65.6-4-6.6-9.6-12.2-16.2-16.2z" class="icconsultoria2"/><path fill="#ffdc64" d="M204.4 42.7h-65.6c-16.2 0-29.4 13.2-29.4 29.4V229c0 16.2 13.2 29.4 29.4 29.4H178l29.4 39.2 29.4-39.2h68.6c16.2 0 29.4-13.2 29.4-29.4v-19.6L204.4 42.7z"/><path fill="#b7e5ff" d="M461.9 222.1h-19.6l-29.4 39.2-29.4-39.2H226.6c-16.2 0-29.4-13.2-29.4-29.4V35.8c0-16.2 13.2-29.4 29.4-29.4h235.3c16.2 0 29.4 13.2 29.4 29.4v156.9c.1 16.2-13.1 29.4-29.4 29.4z"/><path d="M295.2 45.6h156.9v19.6H295.2V45.6zM236.4 84.8h215.7v19.6H236.4V84.8zM236.4 124h215.7v19.6H236.4V124zM236.4 163.2h176.5v19.6H236.4v-19.6zM256 45.6h19.6v19.6H256V45.6z" class="icconsultoria5"/>
            </svg>
          </div>
          <p class="featured-title">Consultoria</p>
          <p class="featured-subtitle">In-company Eventos Vendas</p>
        </div>
        <div class="featured featured-one">
          <div class="featured-background" style="background-image: url('https://server.com/hmi/server/public_html//themes/hmi/resources/img/jakob-dalbjorn-730178-unsplash-x1280-min.jpg')"></div>
          <div class="svg icon">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Layer_1" viewBox="0 0 600 606.8">
              <style>.icartigos1{fill:#99c8e0}.icartigos2{fill:#ffdc64}.icartigos7{fill:#333047}</style><path fill="#b7e5ff" d="M462.4 155.7H355.3c-13.7 0-24.7-11.1-24.7-24.7V20.1l.1-.1-.1-.1H67.9c-13.1 0-25.7 5.2-34.9 14.5-9.3 9.3-14.4 21.9-14.4 35V228l-.1 15v203.9h.1v86.4c0 13.1 5.2 25.6 14.4 34.9 9.3 9.2 21.8 14.4 34.9 14.4h82.3l.1.1H413c13.1 0 25.7-5.2 34.9-14.5 9.3-9.3 14.4-21.9 14.4-35l-.6-1 .7-172.6V155.7zm-78 66.4v112.3h-.1v-66.3H319v-46h65.4zM319 285.8l28.2 48.6H319v-48.6zm-45.7-78.9l-27-46.6h27v46.6zm-65.4 127.5v-66.3H273v66.3h-65.1zm-111.3 0v-66.3h65.3v66.3H96.6z"/><defs><path id="SVGID_1_" d="M18.5 19.8H581v636.4H18.5z"/></defs><clipPath id="SVGID_2_"><use overflow="visible" xlink:href="#SVGID_1_"/></clipPath><path d="M330.7 20l131.6 135.6-.1.1h-107c-13.7 0-24.7-11.1-24.7-24.7l.2-111zm0 0" class="icartigos1"/><path d="M318.8 222.1h65.4v112.3h-65.4zM207.7 160.4h65.4v174.1h-65.4zM96.6 259.2H162v75.3H96.6z" class="icartigos2"/><path d="M294.4 500.5H94.6c-5.2 0-9.4-4.2-9.4-9.4s4.2-9.4 9.4-9.4h199.8c5.2 0 9.4 4.2 9.4 9.4-.1 5.2-4.3 9.4-9.4 9.4zm0 0M264.7 433.9H94.6c-5.2 0-9.4-4.2-9.4-9.4s4.2-9.4 9.4-9.4h170.2c2.5 0 4.9 1 6.6 2.7 1.8 1.8 2.7 4.1 2.7 6.6s-1 4.9-2.8 6.6c-1.7 2-4.1 2.9-6.6 2.9zm0 0" class="icartigos1"/><path fill="#a8ab3b" d="M495.2 333.2v-74.9l10.7-32.1h21.4l10.7 32.1v74.9h-42.8zm0 0"/><path fill="gray" d="M495.2 333.2v-74.9l10.7-32.1h21.4l10.7 32.1v74.9h-42.8zm0 0"/><path fill="#999" d="M505.9 226.3l-10.7 32.1v74.9h21.4v-74.9l-10.7-32.1zm0 0"/><path fill="#911d22" d="M538 493.7V333.2h-42.8v192.5l21.4 42.8 21.4-42.8v-32zm0 0"/><path d="M538 493.7V333.2h-42.8v192.5l21.4 42.8 21.4-42.8v-32zm0 0" class="icartigos7"/><path fill="#77657e" d="M505.9 547.1l10.7-21.4V333.2h-21.4v192.5l10.7 21.4zm0 0"/><path d="M505.9 194.2h21.4v32.1h-21.4zM580.8 343.9h-21.4v-64.2H538v-21.4h32.1c5.9 0 10.7 4.8 10.7 10.7v74.9zm0 0" class="icartigos7"/><path d="M559.4 236.9h21.4V269h-21.4z" class="icartigos7"/>
            </svg>
          </div>
          <p class="featured-title">Artigos</p>
          <p class="featured-subtitle">In-company Eventos Vendas</p>
        </div>
      </div>
    </div>
  </section>

  <section class="section white section-three">
    <div class="section-wrapper">
      <div class="row small-pad">
        <div class="row-titles">
          <p class="row-title">Artigos e Notícias</p>
        </div>
        <div class="content">
          <div class="component-news small">
            <a class="news-picture" href="#" style="background-image: url('https://server.com/hmi/server/public_html//themes/hmi/resources/img/jakob-dalbjorn-730178-unsplash-x1280-min.jpg');"></a>
            <p class="news-title">Gestão para líderes: um começo simples</p>
            <p class="news-description">Adriana fala sobre sua experiência em gestão para líderes: um começo simples</p>
            <a href="#" class="btn">Saiba mais</a>
          </div>
          <div class="component-news small">
            <a class="news-picture" href="#" style="background-image: url('https://server.com/hmi/server/public_html//themes/hmi/resources/img/jakob-dalbjorn-730178-unsplash-x1280-min.jpg');"></a>
            <p class="news-title">Gestão para líderes: um começo simples</p>
            <p class="news-description">Adriana fala sobre sua experiência em gestão para líderes: um começo simples</p>
            <a href="#" class="btn">Saiba mais</a>
          </div>
          <div class="component-news small">
            <a class="news-picture" href="#" style="background-image: url('https://server.com/hmi/server/public_html//themes/hmi/resources/img/jakob-dalbjorn-730178-unsplash-x1280-min.jpg');"></a>
            <p class="news-title">Gestão para líderes: um começo simples</p>
            <p class="news-description">Adriana fala sobre sua experiência em gestão para líderes: um começo simples</p>
            <a href="#" class="btn">Saiba mais</a>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-sections'); ?>
  ##parent-placeholder-2b652bef4ae9be2d3a3fe2f169e6a106e1d3faee##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
  ##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>