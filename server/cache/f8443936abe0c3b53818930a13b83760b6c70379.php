<head>
	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<?php $__env->startSection('vendor-css'); ?>
		<!-- Vendor CSS -->
		<link rel="stylesheet" type="text/css" href="https://server.com/mailsender/server/public_html//themes/mailsender/vendor/normalize/normalize.css">
		<link rel="stylesheet" type="text/css" href="https://server.com/mailsender/server/public_html//themes/mailsender/vendor/hamburgers/hamburgers.min.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('application-css'); ?>
		<!-- Application CSS -->
		<link rel="stylesheet" type="text/css" href="https://server.com/mailsender/server/public_html//themes/mailsender/resources/css/navbar.css">
		<link rel="stylesheet" type="text/css" href="https://server.com/mailsender/server/public_html//themes/mailsender/resources/css/hero.css">
		<link rel="stylesheet" type="text/css" href="https://server.com/mailsender/server/public_html//themes/mailsender/resources/css/section.css">
		<link rel="stylesheet" type="text/css" href="https://server.com/mailsender/server/public_html//themes/mailsender/resources/css/main.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('fonts'); ?>
		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="https://server.com/mailsender/server/public_html//themes/mailsender/resources/fonts/opensans/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="https://server.com/mailsender/server/public_html//themes/mailsender/resources/fonts/vollkorn/stylesheet.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('js'); ?>
		<script src="https://server.com/mailsender/server/public_html//themes/mailsender/vendor/modernizr/modernizr-custom.js"></script>
		<?php echo $__env->yieldSection(); ?>
</head>