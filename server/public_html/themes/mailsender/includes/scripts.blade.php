@section('scripts')
  <!-- Scripts -->
  <!--<script type="text/javascript" src="@asset('vendor/tiny-slider/min/tiny-slider.helper.ie8.js')"></script>-->
  <script type="text/javascript">
    const hamburger = document.querySelector('.hamburger')
    const navbarMenu = document.querySelector('.navbar-menu')

    hamburger.addEventListener('click', function (event) {
      hamburger.classList.toggle('is-active')
      navbarMenu.classList.toggle('active')
    })
  </script>
  @show