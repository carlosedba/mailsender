@section('footer')
	<footer class="footer">
		<div class="footer-sections">
			@section('footer-sections')
				@show
		</div>
		<div class="footer-content">
			<div class="footer-content-left">
				<a class="footer-logo" href="#"></a>
				<div class="footer-info">
					<p class="footer-info-title"></p>
					<p class="footer-info-description"></p>
					<p class="footer-info-address"></p>
				</div>
			</div>
			<div class="footer-content-right"></div>
		</div>
	</footer>
	@show