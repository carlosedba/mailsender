const Tabs = SistemaFiep.prototype.Tabs = function () {
	console.log('log > SistemaFiep > Tabs - initialized!')

	this.windowWidth = window.innerWidth
	
	this.states = ['tab-fiep', 'tab-sesi', 'tab-senai', 'tab-iel']

	this.Event = new window.XEvent()
	this.Event.addHandler('handleMobileClick', this.handleMobileClick, this)
	this.Event.addHandler('handleHighResClick', this.handleHighResClick, this)

	document.addEventListener('DOMContentLoaded', this.render.bind(this))
	window.addEventListener('resize', this.render.bind(this))
}

Tabs.prototype.resetState = function () {
	let tabsController = document.querySelector('.tabs-controller')
	let tabs = document.querySelectorAll('.tabs-controller-item')

	;[].forEach.call(tabs, function (el, i) {
		el.classList.remove('active')
	})
	
	tabsController.classList.add('tab-fiep')
	
	if (!Modernizr.mq('(max-width: 979px)')) tabs[0].classList.add('active')
}

Tabs.prototype.clearState = function () {
	let tabsController = document.querySelector('.tabs-controller')
	let tabs = document.querySelectorAll('.tabs-controller-item')

	;[].forEach.call(tabs, function (el, i) {
		el.classList.remove('active')
	})

	this.states.forEach(function (el, i) {
		tabsController.classList.remove(el)
	})
}

Tabs.prototype.handleMobileClick = function (event) {
	let tab = event.target

	while (!tab.classList.contains('tabs-controller-item')) {
		tab = tab.parentNode
	}

	tab.classList.toggle('active')
}

Tabs.prototype.handleHighResClick = function (event) {
	let tabsController = document.querySelector('.tabs-controller')
	let tab = event.target

	while (tab.tagName !== 'DIV') {
		tab = tab.parentNode
	}

	while (!tab.classList.contains('tabs-controller-item')) {
		tab = tab.parentNode
	}

	if (!tab.classList.contains('active')) {
		this.clearState()
		tab.classList.add('active')
		this.states.forEach(function (el, i) {
			if (tab.classList.contains(el)) {
				tabsController.classList.add(el)
			}
		})
	}
}

Tabs.prototype.render = function (event) {
	console.log('log > SistemaFiep > Tabs - render called!')

	if (Utils.shouldRender(this.windowWidth, event)) {
		console.log('log > SistemaFiep > Tabs - render approved!')

		let tabs = document.querySelectorAll('.tabs-controller-item')

		if (tabs.length) {
			if (Modernizr.mq('(max-width: 979px)')) {
				console.log('log > SistemaFiep > Tabs - events added!')
				this.resetState()
				this.Event.removeFrom(tabs, 'click', 'handleHighResClick')
				this.Event.addTo(tabs, 'click', 'handleMobileClick')
			} else {
				console.log('log > SistemaFiep > Tabs - events removed!')
				this.resetState()
				this.Event.removeFrom(tabs, 'click', 'handleMobileClick')
				this.Event.addTo(tabs, 'click', 'handleHighResClick')
			}
		}
	}
}

