const FooterMenu = SistemaFiep.prototype.FooterMenu = function () {
	console.log('log > SistemaFiep > FooterMenu - initialized!')

	this.windowWidth = window.innerWidth

	this.Event = new window.XEvent()
	this.Event.addHandler('handleClick', this.handleClick)

	document.addEventListener('DOMContentLoaded', this.render.bind(this))
	window.addEventListener('resize', this.render.bind(this))
}

FooterMenu.prototype.clearFooterMenu = function () {
	let body = document.body
	let menu = document.querySelectorAll('.footer-menu')

	;[].forEach.call(menu, function (el, i) {
		el.classList.remove('open')
	})
}

FooterMenu.prototype.handleClick = function (event) {
	event.preventDefault()
	let target = event.target.parentNode

	if (event.target.href === 'javascript:void(0)') {
		target.classList.toggle('open')
	} else {
		location.href = event.target.href
	}
}

FooterMenu.prototype.render = function (event) {
	console.log('log > SistemaFiep > FooterMenu - render called!')
	
	if (Utils.shouldRender(this.windowWidth, event)) {
		console.log('log > SistemaFiep > FooterMenu - render approved!')

		let menuItems = document.querySelectorAll('.footer-menu-title')

		if (Modernizr.mq('(max-width: 979px)')) {
			console.log('log > SistemaFiep > FooterMenu - events added!')
			this.Event.addTo(menuItems, 'click', 'handleClick')
			this.clearFooterMenu()
		} else {
			console.log('log > SistemaFiep > FooterMenu - events removed!')
			this.Event.removeFrom(menuItems, 'click', 'handleClick')
		}
	}
}

