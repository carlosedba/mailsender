const Loading = window.Loading = function () {
	console.log('log > Loading - initialized!')
}

Loading.activate = function () {
	const loading = document.querySelector('.loading')
	loading.classList.add('visible')
}

Loading.deactivate = function () {
	const loading = document.querySelector('.loading')
	loading.classList.remove('visible')
}
