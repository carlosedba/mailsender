// *************************************************** //
// * Sistema Fiep First Week ************************* //
// *************************************************** //

const SistemaFiepFW = (function () {
	'use strict'
		
	let defaults = {}
	
	let api = {
		init: init
	}

	function setWidgetNoticias() {
		let widgets = document.querySelectorAll('.widget-noticias-items')

		if (widgets.length) {
			;[].forEach.call(widgets, function (el, i) {
				let loop = setInterval(loopWidgetNoticias, 5000)
			})
		}
	}

	function loopWidgetNoticias() {
		let noticias = document.querySelectorAll('.widget-noticias-item')

		if (noticias.length) {
			let currentIndex = null
			let currentItem = null
			let nextPicture = null
			let nextItem = null
			let cover = document.querySelector('.widget-noticias-cover')

			;[].forEach.call(noticias, function (el, i, arr) {
				if (el.classList.contains('active')) {
					currentIndex = i
					currentItem = el
				}
			})

			if (currentIndex < noticias.length - 1) {
				nextItem = noticias[currentIndex+1]
			} else {
				nextItem = noticias[0]
			}

			nextPicture = nextItem.querySelector('.widget-noticias-item-picture').dataset.src

			cover.style.backgroundImage = 'url(' + nextPicture + ')'
			currentItem.classList.remove('active')
			nextItem.classList.add('active')
		}
	}

	function setWidgetVideos() {
		let widgets = document.querySelectorAll('.widget-videos-items')
		let items = document.querySelectorAll('.widget-videos-item')
		let thumbnails = document.querySelectorAll('.widget-videos-thumbnail')

		if (widgets.length) {
			;[].forEach.call(widgets, function (el, i) {
				new Flickity(el, {
					groupCells: true
				})
			})
		}

		if (thumbnails.length) {
			;[].forEach.call(thumbnails, function (el, i) {
				el.addEventListener('click', function (e) {
					location.href = items[i].dataset.url
				})
			})
		}
	}

	function setInstitucionalCarousel() {
		let campanhas = document.querySelectorAll('.institucional-campanha')
		let carousels = document.querySelectorAll('.institucional-carousel-items')
		let items = document.querySelectorAll('.institucional-carousel-item')
		let titles = document.querySelectorAll('.institucional-carousel-title')
		let thumbnails = document.querySelectorAll('.institucional-carousel-thumbnail')
		let buttons = document.querySelectorAll('.institucional-campanha-buttons .btn-campanha')

		if (carousels.length) {
			new Flickity(carousels[0], {
				groupCells: true,
				pageDots: false,
			})


        	if (window.innerWidth < 768) {
				;[].forEach.call(carousels, function (el, i) {
					new Flickity(el, {
						groupCells: true,
						pageDots: false,
					})
				})
        	}
		}

		if (items.length) {
			;[].forEach.call(items, function (el, i) {
				let url = el.dataset.url
				if (url !== '') {
					titles[i].addEventListener('click', function (e) {
						e.preventDefault()
						window.open(url, '_blank')
					})
				} else {
					titles[i].addEventListener('click', function (e) {
						e.preventDefault()
						location.href = 'javascript:void(0)'
					})
				}
			})
		}

		if (buttons.length) {
			;[].forEach.call(buttons, function (el, i) {
				let index = el.dataset.index

				el.addEventListener('click', function (e) {
					let selectedCampanhaIndex

					;[].forEach.call(campanhas, function (el, i) {
						if (el.dataset.index === index) {
							selectedCampanhaIndex = i
						}
						el.style.display = 'none'
					})

					campanhas[selectedCampanhaIndex].style.display = 'flex'

					new Flickity(carousels[selectedCampanhaIndex], {
						groupCells: true,
						pageDots: false,
					})

					console.log('index', index)
					console.log('selectedCampanhaIndex', selectedCampanhaIndex)
				})
			})
		}
	}

	function addTags() {
		let play = document.querySelector('.video-play-button')
		let revista = document.querySelectorAll('.widget-revista .btn')

		if (play !== null && play !== undefined) {
			play.addEventListener('click', function (e) {
				try { nvg45975.conversion('V�deo iniciado') } catch (err) { console.log(err) }
			})
		}

		if (revista.length) {
			;[].forEach.call(revista, function (el, i) {
				el.addEventListener('click', function (e) {
					try { nvg45975.conversion('Revista aberta') } catch (err) { console.log(err) }
				})
			})
		}
	}
	
	function init(options) {
		Utils.extend(defaults, options)
		setWidgetNoticias()
		setWidgetVideos()
		setInstitucionalCarousel()
		addTags()
	}

	init()
})()