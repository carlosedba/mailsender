const WebP = SistemaFiep.prototype.WebP = function () {
	console.log('log > SistemaFiep > WebP - initialized!')

	this.windowWidth = window.innerWidth

	this.Event = new window.XEvent()
	this.Event.addHandler('handleSelectGrupoListagem', this.handleSelectGrupoListagem)

	document.addEventListener('DOMContentLoaded', this.render.bind(this))
	//window.addEventListener('resize', this.render.bind(this))
}

WebP.prototype.postPaginatorAlpha = function () {
	let paginator = document.querySelector('.post-paginator-alpha')

	if (paginator !== null) {
		let pages = paginator.querySelectorAll('.post-paginator-item')

		;[].forEach.call(pages, function (el, i) {
			let page = el.dataset.value

			if (page.length) {
				let componentPid = Utils.getQueryParam('componentPid')
				el.href = location.origin + location.pathname + '?componentPid=' + componentPid + '&pageNumber=' + page
			}
		})
	}
}

WebP.prototype.selectGrupoListagem = function () {
	let select = document.querySelectorAll('.select-grupo-listagem')
	let options = document.querySelectorAll('.select-grupo-listagem option')

	;[].forEach.call(options, function (el, i) {
		if (el.value === Utils.getQueryParam('groupPid')) el.selected = true
	})

	this.Event.addTo(select, 'change', 'handleSelectGrupoListagem')
}

WebP.prototype.handleSelectGrupoListagem = function (event) {
	let pageFilter = event.target.parentNode.parentNode.parentNode
	let componentPid = pageFilter.querySelector('.componentPid').value
	let groupPid = event.target.value

	location.href = 'http://' + location.host + location.pathname + '?componentPid=' + componentPid + '&pageNumber=1&groupPid=' + groupPid
}

WebP.prototype.render = function (event) {
	console.log('log > SistemaFiep > WebP - render called!')

	if (Utils.shouldRender(this.windowWidth, event)) {
		console.log('log > SistemaFiep > WebP - render approved!')

		this.postPaginatorAlpha()
		this.selectGrupoListagem()
	}
}

