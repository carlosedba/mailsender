<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Rise\Netcore;
use Rise\Theme;

class Addon
{
	private $net;
	
	function __construct(Netcore $netcore)
	{
		$this->net = $netcore;

		$this->loadRoutes();
	}

	public function loadRoutes()
	{
		$this->net->get('/lalala', function (Request $request, Response $response, $args) {
			$response->getBody()->write('wooooooooooooooww');
			return $response;
		});
	}
}
?>