CREATE DATABASE IF NOT EXISTS `mailsender` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mailsender`;

CREATE TABLE IF NOT EXISTS `rise_ms_users` (
	`id`						varchar(255)  NOT NULL,
	`first_name`		varchar(255)	NOT NULL,
	`last_name`			varchar(255),
	`email`					varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_ms_groups` (
	`id`						varchar(255)  NOT NULL,
	`slug`					varchar(255),
	`data`					longtext,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_ms_emails` (	
	`id`						varchar(255)	NOT NULL,
	`name`					varchar(255) 	NOT NULL,
	`subject`				varchar(255) 	NOT NULL,
	`sender_name`		varchar(255),
	`sender_email`	varchar(255),
	`content`				longtext,
	`slug`					varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_ms_links` (	
	`id`						varchar(255)	NOT NULL,
	`ms_email_id`		varchar(255)	NOT NULL,
	`name`					varchar(255) 	NOT NULL,
	`description`		varchar(255),
	`slug`					varchar(255),
	`content`				longtext,
	`created_at`  	datetime			NOT NULL,
	`updated_at` 		datetime			NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`ms_email_id`) REFERENCES `rise_ms_emails` (`id`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_ms_accesses` (	
	`id`						varchar(255)	NOT NULL,
	`ms_link_id`		varchar(255)	NOT NULL,
	`ms_user_id`		varchar(255) 	NOT NULL,
	`accessed_at`  	datetime			NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`ms_link_id`) REFERENCES `rise_ms_links` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`ms_user_id`) REFERENCES `rise_ms_users` (`id`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_ms_views` (	
	`id`						varchar(255)	NOT NULL,
	`ms_email_id`		varchar(255)	NOT NULL,
	`ms_user_id`		varchar(255) 	NOT NULL,
	`accessed_at`  	datetime			NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`ms_email_id`) REFERENCES `rise_ms_emails` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`ms_user_id`) REFERENCES `rise_ms_users` (`id`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_config` (
	`id`						int 					NOT NULL 	AUTO_INCREMENT,
	`name`					varchar(255) 	NOT NULL,
	`value`					longtext 			NOT NULL,
	`autoload`			bool 					NOT NULL,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_collections` (	
	`id`					varchar(255)	NOT NULL,
	`name`				varchar(255) 	NOT NULL,
	`description`	varchar(255),
	`slug`				varchar(255) 	NOT NULL,
	`picture`			varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_field_types` (
	`id`					int 			NOT NULL 	AUTO_INCREMENT,
	`name`				varchar(255) 	NOT NULL,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_fields` (
	`id`							varchar(255)	NOT NULL,
	`name`						varchar(255) 	NOT NULL,
	`description`			varchar(255),
	`attribute`				varchar(255) 	NOT NULL,
	`field_type_id`		int 					NOT NULL,
	`collection_id`		varchar(255)	NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`field_type_id`) REFERENCES `rise_field_types` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`collection_id`) REFERENCES `rise_collections` (`id`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_content` (
	`id`							varchar(255)	NOT NULL,
	`data`						longtext			NOT NULL,
	`collection_id`		varchar(255) 	NOT NULL,
	FOREIGN KEY (`collection_id`) REFERENCES `rise_collections` (`id`) ON DELETE CASCADE,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_pages` (
	`id`							varchar(255)	NOT NULL,
	`name`						varchar(255)	NOT NULL,
	`slug`						varchar(255)	NOT NULL,
	`collection_id`		varchar(255) 	NOT NULL,
	FOREIGN KEY (`collection_id`) REFERENCES `rise_collections` (`id`) ON DELETE CASCADE,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_users` (
	`id`						varchar(255)  NOT NULL,
	`first_name`		varchar(255)	NOT NULL,
	`last_name`			varchar(255)	NOT NULL,
	`email`					varchar(255)	NOT NULL,
	`password`			varchar(255)	NOT NULL,
	`picture`				varchar(255),
	`role`					varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;


INSERT INTO `rise_users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`, `role`) VALUES ('0cbdeb00', 'Carlos Eduardo', 'Barbosa de Almeida', 'carlosedba@outlook.com', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'eu.jpg', 'admin');
INSERT INTO `rise_users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`, `role`) VALUES ('1cbdeb11', 'Priscila', 'Aguiar', 'priscila.aguiar@sistemafiep.org.br', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'eu.jpg', 'admin');

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_url', 'https://server.com/mailsender/server/public_html/', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_home', 'https://server.com/mailsender/server/public_html/', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_name', 'Mailsender', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_description', '', true);

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('theme', 'mailsender', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('active_addons', '[{"active": true, "name": "rise-pages", }, {"active": true, "name": "rise-posts", }, ]', true);

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('admin_email', 'carlosedba@outlook.com', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('timezone_string', 'America/Sao_Paulo', true);

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_url', '', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_login', '', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_pass', '', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_port', '', true);

INSERT INTO `rise_field_types` (`name`) VALUES ('Texto');
INSERT INTO `rise_field_types` (`name`) VALUES ('Editor de texto');
INSERT INTO `rise_field_types` (`name`) VALUES ('Imagem');
INSERT INTO `rise_field_types` (`name`) VALUES ('Arquivo');
INSERT INTO `rise_field_types` (`name`) VALUES ('Tags');
INSERT INTO `rise_field_types` (`name`) VALUES ('Categoria');
INSERT INTO `rise_field_types` (`name`) VALUES ('Data');
INSERT INTO `rise_field_types` (`name`) VALUES ('Hora');

INSERT INTO `rise_collections` (`id`, `name`, `description`, `slug`, `picture`) VALUES ('0cbdeb00', 'Páginas', '', 'pages', '');
INSERT INTO `rise_collections` (`id`, `name`, `description`, `slug`, `picture`) VALUES ('1cbdeb11', 'Posts', '', 'posts', '');

INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('0cbdeb00', 'Título', '', 'title', '1', '0cbdeb00');
INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('1cbdeb00', 'Conteúdo', '', 'content', '2', '0cbdeb00');

INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('2cbdeb00', 'Título', '', 'title', '1', '0cbdeb00');
INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('3cbdeb00', 'Data', '', 'date', '6', '0cbdeb00');
INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('4cbdeb00', 'Categoria', '', 'category', '1', '0cbdeb00');
INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('5cbdeb00', 'Conteúdo', '', 'content', '2', '0cbdeb00');

/* INSERT INTO `rise_pages` (`id`, `name`, `layout`, `slug`, `collection_id`) VALUES ('0cbdeb00', 'Páginas de conteúdo', 'page', 'pages', '0cbdeb00');
INSERT INTO `rise_pages` (`id`, `name`, `layout`, `slug`, `collection_id`) VALUES ('0cbdeb00', 'Lista de posts', 'posts', 'posts', '1cbdeb11'); */
