const fs = require('fs')
const path = require('path')
const xlsx = require('node-xlsx')
const nodemailer = require('nodemailer')
const cheerio = require('cheerio')

function wait(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, ms)
  })
}

function loadHTML(path) {
  const rawFile = fs.readFileSync(path, 'utf8')
  const $ = cheerio.load(rawFile)

  return $.html()
}

function getMailing(path) {
  let workSheetsFromFile = xlsx.parse(path)
  let length = workSheetsFromFile[0].data.length
  let data = workSheetsFromFile[0].data
  let arr = []

  for (var i = 1; i < length; i++) {
    if (data[i][0]) {
      arr.push(data[i][0])
    }
  }

  return arr
}

function splitArr(arr, max) {
  return arr.map(function (item, index) {
      return index % max === 0 ? arr.slice(index, index + max) : null
  }).filter(function (item) {
    return item
  })
}

async function sendEmailSeq(props, emkt, mailing) {
  if (props && emkt && mailing) {
    for (let i = 0; i < mailing.length; i++) {
      const email = mailing[i]

      props.to = email

      await mail(emkt, props, i).catch(console.error)
      await wait(1500)
    }
  }
}

async function sendEmailMaxMinute(props, emkt, mailing) {
  if (props && emkt && mailing) {
    const maxPerMinute = 30
    const groups = splitArr(mailing, maxPerMinute)

    for (let i = 0; i < groups.length; i++) {
      const currentGroup = groups[i]

      for (let email of currentGroup) {
        props.to = email

        mail(emkt, props).catch(console.error)
      }

      if (currentGroup.length === maxPerMinute) await wait(60000)
    }
  }
}

async function sendEmailBcc(props, emkt, mailing) {
  if (props && emkt && mailing) {

    props.bcc = mailing

    await mail(emkt, emailProps)
  }
}

async function mail(html, props, counter = 0) {
  let transporter = nodemailer.createTransport({
    host: 'smtp.office365.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'comunicacao.fiep@fiepr.org.br',
      pass: 'MktInst@2019'
    }
  })

  let mailOptions = {
    from: `"${props.sender_name}" <${props.sender_email}>`,
    subject: props.subject,
    html: html
  }

  if (props.to) mailOptions.to = props.to

  if (props.bcc) mailOptions.to = props.bcc

  return transporter.sendMail(mailOptions, function (err, info) {
    if (!err) {
      if (typeof mailOptions.to === "string") console.log(`mail > ${counter} > Email sent to ${mailOptions.to}`)
      else mailOptions.to.map(function (email) { console.log(`mail > ${counter} > Email sent to ${email}`) })
    } else {
      console.error(err)
    }
  })
}

function main() {
  const emkt = loadHTML('./avisa-cronograma-fechamento.html')

  const ciIncluirSempre = getMailing('ci-incluir-sempre.xlsx')
  const ciTodosColaboradores = getMailing('ci-todos-colaboradores.xlsx')
  const ciTodosGestores = getMailing('ci-todos-gestores.xlsx')
  const saiuNaImprensa = getMailing('saiu-na-imprensa.xlsx')

  const carlos = ['carlosedba@outlook.com', 'carlos.almeida@sistemafiep.org.br']
  const teste = ['carlosedba@outlook.com', 'carlos.almeida@sistemafiep.org.br', 'graziele.santiago@sistemafiep.org.br', 'vanessa.loreno@sistemafiep.org.br']
  const bruna = ['carlosedba@outlook.com', 'carlos.almeida@sistemafiep.org.br', 'bruna.costa@sistemafiep.org.br']
  const ana = ['carlosedba@outlook.com', 'carlos.almeida@sistemafiep.org.br', 'ana.zettel@sistemafiep.org.br']

  let props = {
    name: 'Integra Avisa | Cronograma de fechamento de final de ano',
    subject: 'Integra Avisa | Cronograma de fechamento de final de ano',
    sender_name: 'Integra',
    sender_email: 'comunicacao.fiep@fiepr.org.br',
  }

  sendEmailSeq(props, emkt, ['carlos.almeida@sistemafiep.org.br', ...teste])
  //sendEmailMaxMinute(props, emkt, carlos)
  //sendEmailBcc(props, emkt, carlos)
}

main()

