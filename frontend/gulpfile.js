'use strict'

const gulp = require('gulp')
const sass = require('gulp-sass')

gulp.task('sass', function () {
	return gulp.src('./src/assets/scss/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./src/assets/css'))
		.pipe(gulp.dest('../scratch/css'))
})

gulp.task('sass:watch', function () {
	gulp.watch('./src/assets/scss/**/*.scss', ['sass'])
})