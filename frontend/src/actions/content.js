import axios from 'axios'
import { HMI_API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function fetchAllContent() {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/content`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.FETCH_ALL_CONTENT,
		payload: request
	}
}

export function fetchContent(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/content/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.FETCH_CONTENT,
		payload: request
	}
}

export function createContent(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/content`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.CREATE_CONTENT,
		payload: request
	}
}

export function updateContent(user, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${HMI_API_ENDPOINT}/content/${user}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.UPDATE_CONTENT,
		payload: request
	}
}

export function deleteContent(user) {
	const request = axios({
		method: 'delete',
		url: `${HMI_API_ENDPOINT}/content/${user}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.DELETE_CONTENT,
		payload: request
	}
}