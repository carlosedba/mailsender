import axios from 'axios'
import { HMI_API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function fetchComponents() {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/components`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.FETCH_COMPONENTS,
		payload: request
	}
}

export function fetchComponent(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/components/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.FETCH_COMPONENT,
		payload: request
	}
}

export function createComponent(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/components`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.CREATE_COMPONENT,
		payload: request
	}
}

export function updateComponent(user, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${HMI_API_ENDPOINT}/components/${user}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.UPDATE_COMPONENT,
		payload: request
	}
}

export function deleteComponent(user) {
	const request = axios({
		method: 'delete',
		url: `${HMI_API_ENDPOINT}/components/${user}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.DELETE_COMPONENT,
		payload: request
	}
}