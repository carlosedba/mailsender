import * as types from '../constants/ActionTypes'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'

import Promise from 'bluebird'
import axios from 'axios'
import store from 'store'
import jwtDecode from 'jwt-decode'

function _fetchToken(dispatch, data) {
  return new Promise((resolve, reject) => {
    dispatch(fetchToken(data)).then(resolve).catch(reject)
  })
}

function _fetchUser(dispatch, token, params) {
  return new Promise((resolve, reject) => {
    dispatch(fetchUser(token, params)).then(resolve).catch(reject)
  })
}

export function fetchToken(data) {
  return {
    type: types.SESSION_FETCH_TOKEN,
    payload: new Promise(function (resolve, reject) {
      resolve({
        data: {
          token: 'token'
        }
      })
    })
  }
}

export function fetchUser(token, params) {
  return {
    type: types.SESSION_FETCH_USER,
    payload: new Promise(function (resolve, reject) {
      resolve({
        data: {
          first_name: 'Carlos Eduardo',
          last_name: 'Barbosa de Almeida',
          email: 'carlosedba@outlook.com',
          picture: 'https://scontent.fbfh3-1.fna.fbcdn.net/v/t1.0-9/29103373_1524373437659906_871079531530682368_n.jpg?_nc_cat=104&_nc_ht=scontent.fbfh3-1.fna&oh=b6bc8b13a7c0c47a11cb1f7b0b0e8dfa&oe=5C648BED'
        }
      })
    })
  }
}

export function login(data) {
  return async dispatch => {
    const fetchTokenResponse = await _fetchToken(dispatch, data).catch(console.log)
    const token = fetchTokenResponse.value.data.token

    if (token) {
      const fetchUserResponse = await _fetchUser(dispatch, token, { id: '1234' }).catch(console.log)
      const userData = fetchUserResponse.value.data
    }

    return fetchTokenResponse
  }
}

export function verifyToken(token) {
  return async dispatch => {
    if (token) {
      const fetchUserResponse = await _fetchUser(dispatch, token, { id: '1234' }).catch(console.log)
      const userData = fetchUserResponse.value.data
    }

    return token
  }
}

export function logout() {
  return async dispatch => {
    store.remove('token')
    store.remove('user')

    dispatch({
      type: types.SESSION_LOGOUT,
      payload: null
    })
  }
}

