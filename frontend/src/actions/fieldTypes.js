import axios from 'axios'
import { API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

let data = [
  { name: 'Texto' },
  { name: 'Editor de texto' },
  { name: 'Imagem' },
  { name: 'Arquivo' },
  { name: 'Tags' },
  { name: 'Categoria' },
  { name: 'Data' },
  { name: 'Hora' },
]

export function fetchFieldTypes() {
  return {
    type: types.FETCH_FIELD_TYPES,
    payload: new Promise(function (resolve, reject) {
      resolve({
        data: data
      })
    })
  }
}

export function fetchFieldType(params) {
  return {
    type: types.FETCH_FIELD_TYPE,
    payload: new Promise(function (resolve, reject) {
      resolve({
        data: data
      })
    })
  }
}

export function createFieldType(props) {
  return {
    type: types.CREATE_FIELD_TYPE,
    payload: new Promise(function (resolve, reject) {
      resolve({
        data: data
      })
    })
  }
}

export function updateFieldType(user, props) {
  return {
    type: types.UPDATE_FIELD_TYPE,
    payload: request
  }
}

export function deleteFieldType(user) {
  return {
    type: types.DELETE_FIELD_TYPE,
    payload: request
  }
}