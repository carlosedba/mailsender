export const PRODUCTION = false
export const SERVER_PORT = location.port
export const SERVER_ADDRESS = (PRODUCTION) ? '' : `https://server.com/hmi/server/public_html/dev.php`
export const API_ENDPOINT = (PRODUCTION) ? '' : `${SERVER_ADDRESS}/api/v1`
export const HMI_API_ENDPOINT = (PRODUCTION) ? '' : `${SERVER_ADDRESS}/api/hmi`

