import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'

import IcChevronDown from '@/icons/ic_chevron_one_down'

export default class Dropdown extends Component {
	constructor(props) {
		super(props)

		this.state = {
			open: false,
		}

		this.handleClick = this.handleClick.bind(this)
	}

	handleClick(event) {
		if (this.state.open) {
	    this.setState((prevState, props) => {
	      return update(prevState, {
	        $merge: { open: false }
	      })
	    })
		} else {
	    this.setState((prevState, props) => {
	      return update(prevState, {
	        $merge: { open: true }
	      })
	    })
		}
	}

	render() {
		return (
      <div className={classNames('dropdown', { open: this.state.open })}>
        <div className="dropdown-header" onClick={this.handleClick}>
          <div className="left">
            <div className="dropdown-title">{this.props.name}</div>
          </div>
          <div className="right">
            <a className={classNames('svg', 'dropdown-arrow', { "arrow-down": !this.state.open, "arrow-up": this.state.open })} href="javascript:void(0)">
            	<IcChevronDown/>
            </a>
          </div>
        </div>
        <div className={classNames('dropdown-content')}>
        	{this.props.children}
        </div>
      </div>
		)
	}
}




