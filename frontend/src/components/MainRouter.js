import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter, HashRouter, Switch, Route, Redirect, Link } from 'react-router-dom'

import AuthorizedRoute from '@/components/AuthorizedRoute'
import UnauthorizedLayout from '@/components/UnauthorizedLayout'
import AuthorizedLayout from '@/components/AuthorizedLayout'

export default class MainRouter extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <AuthorizedRoute path="/control" component={AuthorizedLayout}/>
          <Route path="/" component={UnauthorizedLayout}/>
        </Switch>
      </BrowserRouter>
    )
  }
}

