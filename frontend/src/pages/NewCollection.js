import React, { Component } from 'react'
import { connect } from 'react-redux'

import { fetchUser } from '@/actions/users'

import WidgetFields from '@/components/WidgetFields'

import collection_placeholder from '@/assets/img/collection_placeholder.jpg'

@connect((state) => {
  return {
    token: state.session.auth.token,
  }
}, (dispatch, ownProps) => {
  return {
    fetchUser(params) {
      return dispatch(fetchUser(params))
    },
  }
})
export default class NewCollection extends Component {
  constructor(props) {
    super(props)

    this.previewPicture = React.createRef()
    this.inputPicture = React.createRef()

    this.state = {
      pageWidget: {
        name: '',
        description: '',
        picture: undefined,
      }
    }

    this.handleNameChange = this.handleNameChange.bind(this)
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
    this.handlePictureChange = this.handlePictureChange.bind(this)
  }

  componentDidMount() {}

  handleNameChange(event) {}

  handleDescriptionChange(event) {}

  handlePictureChange(event) {
    event.preventDefault()

    const previewPicture = this.previewPicture.current
    const inputPicture = this.inputPicture.current

    for (let i = 0; i < inputPicture.files.length; i++) {
      if (i === 0) {
        const file = inputPicture.files[i]

        if (!file.type.startsWith('image/')){ continue }

        const reader = new FileReader()
        reader.onload = function (event) {
          previewPicture.style.backgroundImage = `url(${event.target.result})`
        }
        reader.readAsDataURL(file)
      }
    }
  }

  render() {
    return (
      <div className="page page-primary">
        <div className="page-header">
          <p className="page-title">Nova coleção</p>
        </div>
        <div className="page-widget">
          <div className="page-widget-header">
            <div className="page-widget-header-row">
              <div className="left">
                <p className="page-widget-title">Informações básicas</p>
              </div>
              <div className="right"></div>
            </div>
            <div className="primary-widget-header-row no-pad"></div>
          </div>
          <div className="page-widget-content">
            <div className="left">
              <div className="inputs inputs-alpha">
                <div className="input input-big">
                  <label>Nome</label>
                  <input type="text" value={this.state.pageWidget.name} onChange={this.handleNameChange}/>
                </div>
                <div className="input input-monster">
                  <label>Descrição</label>
                  <input type="text" value={this.state.pageWidget.description} onChange={this.handleDescriptionChange}/>
                </div>
                <div className="input input-big">
                  <label>Permissões</label>
                  <input type="text" name=""/>
                </div>
              </div>
            </div>
            <div className="right">
              <div className="input-picture">
                <div className="preview-picture" style={{ backgroundImage: `url(${collection_placeholder})`}} ref={this.previewPicture}>
                  <div className="preview-picture-overlay"></div>
                  <p className="preview-picture-description">Clique para alterar a foto</p>
                  <input type="file" ref={this.inputPicture} onChange={this.handlePictureChange}/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="page-widgets">
          <WidgetFields/>
        </div>
      </div>
    )
  }
}

