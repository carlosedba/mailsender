import React, { Component } from 'react'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import Dropdown from '@/components/Dropdown'
import WidgetFields from '@/components/WidgetFields'

import collection_placeholder from '@/assets/img/collection_placeholder.jpg'
import picture_placeholder from '@/assets/img/picture_placeholder.png'

@connect((state) => {
  return {
    components: state.components.list,
    content: state.content.list,
  }
})
export default class Control extends Component {
  constructor(props) {
    super(props)

    this.previewPicture = React.createRef()
    this.inputPicture = React.createRef()

    this.handlePictureChange = this.handlePictureChange.bind(this)
  }

  handlePictureChange(event) {
    event.preventDefault()

    const previewPicture = this.previewPicture.current
    const inputPicture = this.inputPicture.current

    for (let i = 0; i < inputPicture.files.length; i++) {
      if (i === 0) {
        const file = inputPicture.files[i]

        if (!file.type.startsWith('image/')){ continue }

        const reader = new FileReader()
        reader.onload = function (event) {
          previewPicture.style.backgroundImage = `url(${event.target.result})`
        }
        reader.readAsDataURL(file)
      }
    }
  }

  render() {
    const oQueOferecemosConfig = {
      source: this.props.content,
      componentId: 3,
      fields: [
        {
          name: 'Nome',
          attribute: 'name',
          percentage: '50%'
        },
        {
          name: 'Descrição',
          attribute: 'description',
          percentage: '50%'
        }
      ],
      methods: {}
    }

    return (
      <div className="page center control-page">
        <div className=""></div>
        <div className="widget widget-alpha">
          <div className="widget-header">
            <p className="widget-title">Página inicial</p>
            <p className="widget-description">Expanda os itens para editar o conteúdo da página.</p>
          </div>
          <div className="widget-content">
            <Dropdown name="Hero">
              <div className="left">
                <div className="inputs">
                  <div className="input-picture-alpha">
                    <label>Imagem:</label>
                    <div className="preview-picture" style={{ backgroundImage: `url(${picture_placeholder})`}} ref={this.previewPicture}>
                      <div className="preview-picture-overlay"></div>
                      <p className="preview-picture-description">Clique para alterar</p>
                      <input type="file" ref={this.inputPicture} onChange={this.handlePictureChange}/>
                    </div>
                  </div>
                </div>
              </div>
              <div className="right">
                <div className="inputs">
                  <div className="input input-4hun">
                    <label>Título:</label>
                    <input type="text"/>
                  </div>
                </div>
              </div>
            </Dropdown>
            <Dropdown name="Quem somos">
              <div className="left">
                <div className="inputs">
                  <div className="input input-monster">
                    <label>Título:</label>
                    <input type="text"/>
                  </div>
                  <div className="input input-monster">
                    <label>Descrição:</label>
                    <textarea></textarea>
                  </div>
                  <span className="inputs-title">Informações do Box</span>
                  <div className="input input-monster">
                    <label>Texto:</label>
                    <textarea></textarea>
                  </div>
                  <div className="input input-monster">
                    <label>Link:</label>
                    <input type="text"/>
                  </div>
                </div>
              </div>
              <div className="right"></div>
            </Dropdown>
            <Dropdown name="O que oferecemos">
              <WidgetFields config={oQueOferecemosConfig}/>
            </Dropdown>
            <Dropdown name="Cases"></Dropdown>
            <Dropdown name="Slider"></Dropdown>
          </div>
        </div>
      </div>
    )
  }
}

