import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import browserStore from 'store'

import { verifyToken } from '@/actions/session'

import MainRouter from '@/components/MainRouter'

import setStore from '@/store'

import './libs/store/store.min.js'
import './libs/normalize/normalize.css'

import './assets/css/boilerplate.global.css'
import './assets/css/common.global.css'
import './assets/css/inputs.global.css'
import './assets/css/buttons.global.css'
import './assets/css/header.global.css'
import './assets/css/widget.global.css'
import './assets/css/dropdown.global.css'

//import './assets/css/buttons.css'
//import './assets/css/modal.css'
//import './assets/css/main.css'
//import './assets/css/colors.css'

function renewTokenIfExists() {
  let token = browserStore.get('token')
  if (token) store.dispatch(verifyToken(token))
}

const store = setStore()

const render = Component => {
  ReactDOM.render(
    <Provider store={store}>
      <Component/>
    </Provider>
    , document.getElementById('root')
  )
}

//renewTokenIfExists()

render(MainRouter)

// webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept('@/components/MainRouter', () => {
    render(MainRouter)
  })
}