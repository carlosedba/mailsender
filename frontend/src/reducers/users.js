import {
	SEARCH_USERS, SEARCH_USERS_LOADING, SEARCH_USERS_SUCCESS, SEARCH_USERS_ERROR,
	FETCH_USERS, FETCH_USERS_LOADING, FETCH_USERS_SUCCESS, FETCH_USERS_ERROR,
	FETCH_USER, FETCH_USER_LOADING, FETCH_USER_SUCCESS, FETCH_USER_ERROR,
	CREATE_USER, CREATE_USER_LOADING, CREATE_USER_SUCCESS, CREATE_USER_ERROR,
	UPDATE_USER, UPDATE_USER_LOADING, UPDATE_USER_SUCCESS, UPDATE_USER_ERROR,
	DELETE_USER, DELETE_USER_LOADING, DELETE_USER_SUCCESS, DELETE_USER_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	search: 		{ items: [], error: null, loading: null },
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case SEARCH_USERS:
		return { ...state, search: { items: [], error: null, loading: null } }

	case SEARCH_USERS_LOADING:
		return { ...state, search: { items: [], error: null, loading: true } }

	case SEARCH_USERS_SUCCESS:
		return { ...state, search: { items: action.payload.data, error: null, loading: false } }

	case SEARCH_USERS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, search: { items: [], error: error, loading: false } }


	case FETCH_USERS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_USERS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_USERS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_USERS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_USER:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_USER_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_USER_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_USER_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_USER:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_USER_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_USER_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_USER_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_USER:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_USER_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_USER_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_USER_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_USER:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_USER_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_USER_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_USER_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
