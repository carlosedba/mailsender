import { combineReducers } from 'redux'

import session from './session'
import users from './users'
import components from './components'
import content from './content'

const reducer = combineReducers({
  session,
  users,
  components,
  content,
})

export default reducer

