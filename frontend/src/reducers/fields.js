import {
	FETCH_FIELDS, FETCH_FIELDS_LOADING, FETCH_FIELDS_SUCCESS, FETCH_FIELDS_ERROR,
	FETCH_FIELD, FETCH_FIELD_LOADING, FETCH_FIELD_SUCCESS, FETCH_FIELD_ERROR,
	CREATE_FIELD, CREATE_FIELD_LOADING, CREATE_FIELD_SUCCESS, CREATE_FIELD_ERROR,
	UPDATE_FIELD, UPDATE_FIELD_LOADING, UPDATE_FIELD_SUCCESS, UPDATE_FIELD_ERROR,
	DELETE_FIELD, DELETE_FIELD_LOADING, DELETE_FIELD_SUCCESS, DELETE_FIELD_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_FIELDS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_FIELDS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_FIELDS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_FIELDS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_FIELD:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_FIELD_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_FIELD_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_FIELD_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_FIELD:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_FIELD_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_FIELD_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_FIELD_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_FIELD:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_FIELD_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_FIELD_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_FIELD_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_FIELD:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_FIELD_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_FIELD_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_FIELD_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
