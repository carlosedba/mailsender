import {
	FETCH_PAGES, FETCH_PAGES_LOADING, FETCH_PAGES_SUCCESS, FETCH_PAGES_ERROR,
	FETCH_PAGE, FETCH_PAGE_LOADING, FETCH_PAGE_SUCCESS, FETCH_PAGE_ERROR,
	CREATE_PAGE, CREATE_PAGE_LOADING, CREATE_PAGE_SUCCESS, CREATE_PAGE_ERROR,
	UPDATE_PAGE, UPDATE_PAGE_LOADING, UPDATE_PAGE_SUCCESS, UPDATE_PAGE_ERROR,
	DELETE_PAGE, DELETE_PAGE_LOADING, DELETE_PAGE_SUCCESS, DELETE_PAGE_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_PAGES:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_PAGES_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_PAGES_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_PAGES_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_PAGE:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_PAGE_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_PAGE_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_PAGE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_PAGE:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_PAGE_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_PAGE_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_PAGE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_PAGE:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_PAGE_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_PAGE_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_PAGE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_PAGE:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_PAGE_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_PAGE_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_PAGE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
